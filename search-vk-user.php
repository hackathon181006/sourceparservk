<?php
// Разрешение на отображение ошибок на экране
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('config.php');
require_once('function.php');

$query = $_REQUEST['query'];
$cityId = null;

if (isset($_REQUEST['city'])) {
	$cityQuery = $_REQUEST['city'];
	$citiesResponse = getApiMethod('database.getCities', array(
		'country_id' => 1,
		'q' => $cityQuery,
		'count' => 1
	));	
	$citiesResponseObj = json_decode($citiesResponse, true);
	
	if ($citiesResponseObj['response']['count'] > 0) {
		$cityId = $citiesResponseObj['response']['items'][0]['id'];	
	}
}


$searchParams = array(
	'q' => $query,
	'fields' => 'photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, lists, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me'
);

if (isset($cityId)) {
	$searchParams['city'] = $cityId;
}
	

$response = getApiMethod('users.search', $searchParams);
//echo $response;
$response_obj = json_decode($response, true);

if (isset($response_obj['response'])) {
    if ($response_obj['response']['count'] == 0) {
        unset($searchParams['city']);
        $response = getApiMethod('users.search', $searchParams);
        $response_obj = json_decode($response, true);
    }

	echo json_encode($response_obj['response']);
}

/*
        if (count($retrievedObjects) > 0) { 
            //надо либо все менять, либо только первый. Подумать

            $retrievedObject = $retrievedObjects[0];
            
            switch ($event_item['event']) {             
                case 'delivered':
                    if ($retrievedObject['leadstatus'] !== 'Предложение email заинтересовало')
                        $retrievedObject['leadstatus'] = 'Отправлено предложение по email';
                    break;              
                case 'opened':
                    if ($retrievedObject['leadstatus'] !== 'Предложение email заинтересовало')
                        $retrievedObject['leadstatus'] = 'Предложение email прочитано';
                    break;
                case 'clicked':
                    $retrievedObject['leadstatus'] = 'Предложение email заинтересовало';
                    break;
                case 'unsubscribed':
                    $retrievedObject['leadstatus'] = 'Ненужное Обращение';
                    break;
                case 'undelivered':
                    $retrievedObject['leadstatus'] = 'Предложение не доставлено на email';
                    break;                  
            }
            
            //encode the object in JSON format to communicate with the server.
            $objectJson = Zend_JSON::encode($retrievedObject);

            //sessionId is obtained from the login operation result.
            $params = array("sessionName"=>$sessionId, "operation"=>'update', "element"=>$objectJson);

            //update must be a POST request.
            $httpc->post("$endpointUrl", $params, true);
            $response = $httpc->currentResponse();
            //decode the json encode response from the server.
            $jsonResponse = Zend_JSON::decode($response['body']);
            //var_dump($jsonResponse);
            //operation was successful get the token from the reponse.
            if($jsonResponse['success']==false)
                //handle the failure case.
                die('update failed:' . $jsonResponse['error']['code'] .$jsonResponse['error']['message']);

            //update result object.
            $updatedObject = $jsonResponse['result']; 
            //var_dump($updatedObject );        
        }       
        
  */      



/*
    $request = 'https://api.vk.com/method/users.get?uids=ID&fields=photo_200,status';
    $response = file_get_contents($request);
    echo $response;

    $info = array_shift(json_decode($response)->response);

    var_dump($info->photo_200); // URL фотографии
    var_dump($info->status);    // Статус




    require 'vkapi.class.php'; #путь к файлу vkapi.class.php
    echo "test";

    $api_id = '5639373'; #id приложения
    $secret_key = 'tfjfjFCKPCU9fGOxNh4D'; #секретный ключь приложения
    $user_id = '26590337'; #Ваш ID ВКонтакте
    //включаю библиотеку VK
    $VK = new vkapi($api_id, $secret_key);
    $prof = $VK->api('getProfiles', array('uids' => $user_id, 'fields' =>'first_name,last_name,photo_100,status,screen_name'));
    print_r($prof);
    exit();

    $adsd = sizeOf($prof['response']);
            for ($d = 0; $d < $adsd; $d++) {
    echo '<ul id="id__user__profile">
<li>
    <img src="' . $prof['response'][$d]['photo_100'] . '">
    <a href="http://vk.com/' . $prof['response'][$d]['screen_name'] . '" class="user-name">' . $prof['response'][$d]['first_name'] . ' ' . $prof['response'][$d]['last_name'] . '</a>
    <span class="user-group">' . $prof['response'][$d]['status'] . '</span>
</li> </ul>';}

*/
?>
