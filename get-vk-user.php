<?php
// Разрешение на отображение ошибок на экране
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('config.php');
require_once('function.php');

function parseVkProfiles($networking_id) {
    //$query = $_GET['query'];
/*
    global $model;
    $rows = $model->getNetworkingRows($networking_id);
    if (!isset($rows))
        return null;
*/

    $headers = $rows[0];
    $user_ids = array();
    /*
    for ($user_number = 2; $user_number <= count($rows); $user_number++) {              
        $table_vk = getColumn($rows, $user_number, 'вконтакте');
        if (!empty($table_vk)) {
            array_push($user_ids, getVkId($table_vk));
        }        
    }*/

    $searchParams = array(
        'user_ids' => implode(',', $user_ids),
        'fields' => 'photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, lists, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me'
    );
    //'photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, lists, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me'

    $response = getApiMethod('users.get', $searchParams);
    //echo $response;
    $response_obj = json_decode($response, true);

    if (isset($response_obj['response'])) {                                   
        return $response_obj['response'];
    } else return null;
}


?>
