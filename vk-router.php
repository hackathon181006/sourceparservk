<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1); 

set_time_limit(0);
date_default_timezone_set('UTC');

require_once('function.php');
require_once __DIR__.'/config.php';

if (php_sapi_name() == 'cli-server') {
	$request_uri = strtok($_SERVER["REQUEST_URI"], '?');
	
	switch ($request_uri) {
		case '/get-user':	
			$response = getApiMethod('users.get', array(
				'user_ids' => $_REQUEST['uid'],
				'fields' => 'photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me'
			));
			
			$response_obj = json_decode($response, true);

			if (count($response_obj['response']) > 0) {
			    echo json_encode($response_obj['response'][0]);
			}			
			break;
		case '/search-user':
			$query = $_REQUEST['query'];
			$cityId = null;

			if (isset($_REQUEST['city'])) {
				$cityQuery = $_REQUEST['city'];
				$citiesResponse = getApiMethod('database.getCities', array(
					'country_id' => 1,
					'q' => $cityQuery,
					'count' => 1
				));	

				$citiesResponseObj = json_decode($citiesResponse, true);
				
				if ($citiesResponseObj['response']['count'] > 0) {
					$cityId = $citiesResponseObj['response']['items'][0]['id'];	
				}
			}


			$searchParams = array(
				'q' => $query,
				'fields' => 'photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, lists, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me'
			);

			if (isset($cityId)) {
				$searchParams['city'] = $cityId;
			}
				

			$response = getApiMethod('users.search', $searchParams);
			//echo $response;
			
			$response_obj = json_decode($response, true);

			if (isset($response_obj['response'])) {
			    if ($response_obj['response']['count'] == 0) {
			        unset($searchParams['city']);
			        $response = getApiMethod('users.search', $searchParams);
			        $response_obj = json_decode($response, true);
			    }

				echo json_encode($response_obj['response']['items']);
			}
			break;
		default:
			break;
	}

}

	



?>